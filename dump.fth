: dump ( addr len -- )
	base @ >r
	hex

	begin
		?dup
	while
		over 8 u.r
		space

		2dup
		1- 15 and 1+
		begin
			?dup
		while
			swap
			dup c@
			2 .r space
			1+ swap 1-
		repeat
		drop

		2dup 1- 15 and 1+
		begin
			?dup
		while
			swap
			dup c@
			dup 32 128 within if
				emit
			else
				drop [char] . emit
			then
			1+ swap 1-
		repeat
		drop
		cr

		dup 1- 15 and 1+
		tuck
		-
		>r + r>
	repeat

	drop

	r> base !
;
