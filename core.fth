1 cells constant cell
32      constant bl
0       constant false
-1      constant true

: hex 16 base ! ;
: decimal 10 base ! ;

: within  over - >r - r> u< ;
: 0=     0 =  ;
: 0>     0 >  ;
: 0<     0 <  ;
: 0<>    0 <> ;
: 1-     1 -  ;
: 1+     1 +  ;
: chars       ; immediate
: char+  1+   ;
: cell+  cell + ;
: 2*     1 lshift ;
: /      /mod nip ;
: mod    /mod drop ;
\ : /mod   >r s>d r> sm/rem ;
: */mod  >r m* r> sm/rem ;
: */     */mod nip ;
: s>d    dup 0< ;

: tuck   swap over ;
: rot    >r swap r> swap ;
: 2drop  drop drop ;
: 2dup   over over ;
: 2swap  rot >r rot r> ;
: 2over  3 pick 3 pick ;
: 2>r    swap r> swap >r swap >r >r ;
: 2r>    r> r> swap r> swap >r swap ;
: 2@     >r r@ cell+ @ r> @ ;
: 2!     >r r@ ! r> cell+ ! ;

: (2literal) r@ @ r@ cell+ @ r> 2 cells + >r ; compile-only
: literal    postpone ['] , ;  compile-only immediate
: 2literal   postpone (2literal) swap , , ; compile-only immediate
: compile,   , ;  compile-only

: >mark     here 0 , ;     compile-only
: >resolve  here swap ! ;  compile-only
: <mark     here ;         compile-only
: <resolve  , ;            compile-only

: ahead  postpone branch >mark ;         compile-only immediate
: if     postpone ?branch >mark ;        compile-only immediate
: else   postpone ahead swap >resolve ;  compile-only immediate
: then   >resolve ;                      compile-only immediate

: begin   <mark ;                              compile-only immediate
: again   postpone branch <resolve ;           compile-only immediate
: until   postpone ?branch <resolve ;          compile-only immediate
: while   postpone ?branch >mark swap ;        compile-only immediate
: repeat  postpone branch <resolve >resolve ;  compile-only immediate

: (do)    r>  dup @ >r swap >r swap >r  cell+ >r ; compile-only
: (?do)   2dup = if 2drop r> cell+ @ >r then ; compile-only
: (loop)  r> r> r> 1+ over over = if drop drop drop exit then >r >r >r ;
          compile-only
\ taken from gforth
\ https://git.savannah.gnu.org/cgit/gforth.git/tree/prim
: loop-crossed  ( nlimit incr index1 -- crossed )
                rot - ( incr olddiff )
                2dup + ( incr olddiff tmp )
                over xor ( incr olddiff not-crossed )
                >r xor r> and ( not-crossed-or-wrap )
                0 >= ( crossed ) ;
: (+loop)  r> swap r> swap r> 2dup + >r 2 pick >r loop-crossed
           if >r exit then rdrop rdrop drop ; compile-only
: do      postpone (do) >mark <mark ; compile-only immediate
: ?do     postpone (?do) postpone do ; compile-only immediate
: loop    postpone (loop) postpone branch <resolve >resolve ;
          compile-only immediate
: +loop   postpone (+loop) postpone branch <resolve >resolve ;
          compile-only immediate
: unloop  r> rdrop rdrop rdrop >r ; compile-only
: leave   rdrop rdrop rdrop ; compile-only
: i       r> r> r> dup >r swap >r swap >r ; compile-only
: j       r> r> r> r> i swap >r swap >r swap >r swap >r ; compile-only

: case     0 ; compile-only immediate
: of       postpone over postpone = postpone if postpone drop ;
           compile-only immediate
: endof    postpone else ; compile-only immediate
: endcase  postpone drop begin ?dup while postpone then repeat ;
           compile-only immediate

: abs   dup 0< if negate then ;
: dabs  dup 0< if dnegate then ;
: max   2dup > if drop exit then nip ;
: min   2dup < if drop exit then nip ;

: char   parse-name drop c@ ;
: [char] char postpone literal ; compile-only immediate

: /string  dup >r - swap r> chars + swap ;

\ taken from gforth
: mem,  here over allot swap move ;
: scan  >r begin  dup while  over c@ r@ <> while  1 /string repeat then rdrop ;

: parse  >r source >in @ /string over swap r> scan >r over - dup r>
         if 1+ then >in +! ;
: count  dup 1+ swap c@ ;
: word   leading- parse here c! here count move 0 here count + ! here ;

: sliteral  postpone ahead over >r here >r >r mem, 0 c, align r> postpone then
            r> r> postpone 2literal ; compile-only immediate
: s"        [char] " parse postpone sliteral ; compile-only immediate
: ."        postpone s" postpone type ; compile-only immediate
: cliteral  postpone ahead here >r >r dup c, mem, 0 c, align r> postpone then
            r> postpone literal ; compile-only immediate
: c"        [char] " parse postpone cliteral ; compile-only immediate

: >upper  dup [ char a char z 1+ ] 2literal within
          if [ char A char a - ] literal + then ;

: digit? ( c base -- u t ) >r >upper 48 - 9 over < if 7 - dup 10 < or then
                           dup r> u< ;

: >number ( ud1 c-addr1 u1 -- ud2 c-addr2 u2 )
    begin
        dup
    while
        over c@ base @ digit?
        0= if drop exit then
        >r 2swap base @
        \ ud*
        tuck * >r um* r> +
        r> m+ 2swap
        1 /string
    repeat ;

\ TODO check hld-start == hld-offs (no compilation during <# ... #>
\ TODO check buf overflow ?
variable hld
: pad   here 256 + aligned ; \ what if CHAR_BIT 8 <> ?
: >digit  9 over < [ char A char 9 1 + - ] literal and + [char] 0 + ;
: <#    pad hld ! ; \ hld area is just below
: hold  hld @ 1- dup hld ! c! ;
: #     0 base @ um/mod >r base @ um/mod swap >digit hold r> ;
: #s    begin # 2dup or 0= until ;
: sign  0< if [char] - hold then ;
: #>    2drop hld @ pad over - ;


: cr      10 emit ;
: space   bl emit ;
: spaces  0 max 0 ?do space loop ;

: d.r  >r dup >r dabs <# #s r> sign #> r> over - spaces type ;
: d.   0 d.r space ;
: .    s>d d. ;
: u.   0 d. ;
: .r   >r s>d r> d.r ;
: u.r  0 swap d.r ;
: .s   depth 0 ?do depth 1- i - pick . loop ;

\ TODO handle too long names
\ TODO handle not found
: '  parse-name dup here c! here 1+ swap move here find drop ;

\ TODO interpreter should give those (this is implemntatation detail
hex
80000000 constant FLAG_HIDE
FFFF     constant MASK_NAME
decimal
: ffa    [ 1 cells ] literal + ;
: nfa    [ 2 cells ] literal + ;
: cfa    [ 3 cells ] literal + ;
: pfa    [ 4 cells ] literal + ;
: >body  [ 2 cells ] literal + ;
: visible?  ffa @ FLAG_HIDE and 0= ;
: >name     dup nfa @ swap ffa @ MASK_NAME and ;

: words  last @
         begin ?dup while dup visible? if dup >name type space then @ repeat ;

\ TODO should be with flow control words
: recurse  last @ cfa compile, ; compile-only immediate
: ~        postpone branch last @ pfa , ; compile-only immediate

: fill  >r begin dup while 1- 2dup + r@ swap c! repeat 2drop rdrop ;
