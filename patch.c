/*
 * Patch! Simple, embeddable in applications Forth implementation.
 *
 * Copyright (c) 2024 Michał Kloc
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/* TODO list
 *
 * - vm config
 * - table of contents
 * - words:
 *   + vocabularies
 *   + asm (x86, x86_64, arm, thumb, aarch64)
 *   + floats
 * - linux/posix target (catch signals, memmap, dl)
 * - libc
 * - opengl
 * - emscripten, node target
 * - android target
 * - parse patch file
 * - generate docs from patch file
 *
 * Structure
 *
 * - compile time config (TODO: remove)
 * - api: patch
 * - api: ccall
 * - api: internal
 * - impl: patch core (mandatory)
 * - impl: patch api (mixed with core) (mandatory) (TODO: move below)
 * - impl: ccall (TODO: move ccall primitive here)
 * - impl: lua
 * - impl: patch api (mandatory) TODO
 * - app: basic
 * - app: none
 * - app: build
 */

// TODO: push this to runtime
#define PATCH_PS_CAP 128
#define PATCH_RS_CAP 128

#define PATCH_VER_MAJOR 0
#define PATCH_VER_MINOR 1
#define PATCH_VER_PATCH 1
#define PATCH_VER_BUILD 0

#ifndef PATCH_H
#define PATCH_H

#include <stdnoreturn.h>
#include <stddef.h>
#include <stdint.h>

union patch_cell_t;

typedef void (* patch_op_t)(union patch_cell_t *);

typedef union patch_cell_t
{
    uintptr_t uint;
    intptr_t sint;
    char byte;
    unsigned char ubyte;
    void * obj;
    const void * cobj;
    char * str;
    const char * cstr;
    union patch_cell_t * cell;
    patch_op_t op;
}
patch_cell_t;

void patch_push (patch_cell_t * buffer, patch_cell_t value);

patch_cell_t patch_pop (patch_cell_t * buffer);

size_t patch_depth (patch_cell_t * buffer);

void * patch_alloc (patch_cell_t * buffer, size_t size);

void patch_doxt (patch_cell_t * buffer, const patch_cell_t * xt);

void patch_dostr (patch_cell_t * buffer, const char * str, size_t size);

// TODO: user defined action on empty
void patch_empty (patch_cell_t * buffer);

void patch_init (patch_cell_t * buffer, size_t size);

// user defined

extern noreturn void patch_user_error (patch_cell_t * buffer, intptr_t code);

#endif // PATCH_H

#ifndef PATCH_CCALL_H
#define PATCH_CCALL_H

void patch_ccall (patch_cell_t * stack);

patch_cell_t patch_ccall_1 (patch_cell_t * stack);

#endif // PATCH_CCALL_H

#ifdef PATCH_IMPL

#include <assert.h>
#include <ctype.h>
#include <stdbool.h>
#include <string.h>

#define PRIMITIVE_NAME(name)\
    name ## _word
#define PRIMITIVE(name, fun, flags, prev)\
    static const cell PRIMITIVE_NAME(fun)[] = {\
        {.cobj=prev},{.uint=flags|(sizeof(name)-1)},{.str=name},{.op=fun}};
#define PRIMITIVE_START(name, fun, flags)\
    PRIMITIVE(name, fun, flags, NULL)
#define PRIMITIVE_ADD(name, fun, flags, prev)\
    PRIMITIVE(name, fun, flags, PRIMITIVE_NAME(prev))

typedef patch_cell_t cell;

typedef union dcell
{
    cell cell[2];
#ifdef __x86_64
    unsigned __int128 uint;
    __int128 sint;
#else
    uint64_t uint;
    int64_t  sint;
#endif
}
dcell;

typedef enum patch_var_t
{
    PATCH_VAR_PS0,
    PATCH_VAR_PSP,
    PATCH_VAR_PSE,
    PATCH_VAR_RS0,
    PATCH_VAR_RSP,
    PATCH_VAR_RSE,
    PATCH_VAR_DP,
    PATCH_VAR_DSIZE,
    PATCH_VAR_IP,
    PATCH_VAR_W,
    PATCH_VAR_LAST,
    PATCH_VAR_STATE,
    PATCH_VAR_SOURCE,
    PATCH_VAR_SOURCE_U,
    PATCH_VAR_IN,
    PATCH_VAR_BASE,

    PATCH_VAR_END,

    PATCH_VAR_PS_BEG = PATCH_VAR_END,
    PATCH_VAR_PS_POS = PATCH_VAR_END - 1,
    PATCH_VAR_PS_END = PATCH_VAR_END + PATCH_PS_CAP,

    PATCH_VAR_RS_BEG = PATCH_VAR_PS_END,
    PATCH_VAR_RS_POS = PATCH_VAR_PS_END - 1,
    PATCH_VAR_RS_END = PATCH_VAR_PS_END + PATCH_RS_CAP,

    PATCH_VAR_DATA_SPACE_START,
}
patch_var_t;

typedef enum error_t
{
    ERROR_ABORT        =  -1, // TODO
    ERROR_ABORT2       =  -2, // TODO
    ERROR_PS_OVERFLOW  =  -3,
    ERROR_PS_UNDERFLOW =  -4,
    ERROR_RS_OVERFLOW  =  -5,
    ERROR_RS_UNDERFLOW =  -6,
    ERROR_DI_OVERFLOW  =  -8,
    ERROR_0_DIV        = -10,
    ERROR_UNDEF_WORD   = -13,
    ERROR_CONLY_WORD   = -14,
    ERROR_0_LEN_NAME   = -16,
    ERROR_BAD_ALIGN    = -23,
    ERROR_COMP_NESTING = -29, // TODO
    ERROR_NO_BODY      = -31, // TODO
}
error_t;

typedef enum flag_t
{
    FLAG_IMME = 0x80000000,
    FLAG_COMP = 0x40000000,
    FLAG_HIDE = 0x20000000,
    FLAG_NAME = 0xFFFF,
}
flag_t;

typedef enum state_t
{
    STATE_INTERPR = 0,
    STATE_COMPILE = 1,
}
state_t;

static intptr_t boolfth (bool value)
{
    return -1 * value;
}

static void check (cell * mem, bool failed, intptr_t code)
{
    if (failed)
    {
        patch_user_error(mem, code);
    }
}

static cell * ppush (cell * mem)
{
    check(mem, patch_depth(mem) >= PATCH_PS_CAP, ERROR_PS_OVERFLOW);

    return ++mem[PATCH_VAR_PSP].cell;
}

static cell * ppop (cell * mem)
{
    check(mem, patch_depth(mem) <= 0, ERROR_PS_UNDERFLOW);

    return mem[PATCH_VAR_PSP].cell--;
}

static cell * ppop_frame (cell * mem, size_t count)
{
    check(mem, patch_depth(mem) < count, ERROR_PS_UNDERFLOW);

    return (mem[PATCH_VAR_PSP].cell -= count);
}

static cell * ppeek (cell * mem, size_t index)
{
    check(mem, patch_depth(mem) <= index, ERROR_PS_UNDERFLOW);

    return mem[PATCH_VAR_PSP].cell - index;
}

static size_t rdepth (cell * mem)
{
    return 1 + (mem[PATCH_VAR_RSP].cell - mem[PATCH_VAR_RS0].cell);
}

static cell * rpush (cell * mem)
{
    check(mem, rdepth(mem) >= PATCH_RS_CAP, ERROR_RS_OVERFLOW);

    return ++mem[PATCH_VAR_RSP].cell;
}

static cell * rpop (cell * mem)
{
    check(mem, rdepth(mem) <= 0, ERROR_RS_UNDERFLOW);

    return mem[PATCH_VAR_RSP].cell--;
}

static cell * rpeek (cell * mem, size_t index)
{
    check(mem, rdepth(mem) <= index, ERROR_RS_UNDERFLOW);

    return mem[PATCH_VAR_RSP].cell - index;
}

static cell * next (cell * mem)
{
    return mem[PATCH_VAR_IP].cell++;
}

static char * here (cell * mem)
{
    return mem[PATCH_VAR_DP].str;
}

static cell aligned (cell value)
{
    // TODO: make a test instead of an assert (but assert can stay xD)
    assert(sizeof(value) == 2 || sizeof(value) == 4 || sizeof(value) == 8);
    value.uint = value.uint + (sizeof(value) - 1);
    value.uint = value.uint & ~(sizeof(value) - 1);
    return value;
}

static bool ismisaligned (cell value)
{
    assert(sizeof(value) == 2 || sizeof(value) == 4 || sizeof(value) == 8);

    return value.uint & (sizeof(value) - 1);
}

static bool badallot (cell * mem, size_t size)
{
    char * start = (mem[PATCH_VAR_DP].str + size);
    char * end   = (mem[PATCH_VAR_DSIZE].uint + (char *) mem);

    // TODO: UB (must not create pointer outside range)
    return (start >= end);
}

static void align (cell * mem)
{
    mem[PATCH_VAR_DP] = aligned(mem[PATCH_VAR_DP]);
}

static cell * compile (cell * mem)
{
    check(mem, ismisaligned(mem[PATCH_VAR_DP]), ERROR_BAD_ALIGN);
    check(mem, badallot(mem, sizeof(cell)), ERROR_DI_OVERFLOW);

    return mem[PATCH_VAR_DP].cell++;
}

static char * compile_byte (cell * mem)
{
    check(mem, badallot(mem, sizeof(char)), ERROR_DI_OVERFLOW);

    return mem[PATCH_VAR_DP].str++;
}

static void * allot (cell * mem, intptr_t size)
{
    void * result = mem[PATCH_VAR_DP].str;

    check(mem, badallot(mem, size), ERROR_DI_OVERFLOW);

    mem[PATCH_VAR_DP].str += size;

    return result;
}

static void compile_str (cell * mem, const char * str, size_t size)
{
    char * dest = allot(mem, size + 1);

    memcpy(dest, str, size);
    dest[size] = '\0';
}

static void setinput (cell * mem, const char * str, size_t size)
{
    mem[PATCH_VAR_SOURCE].cstr = str;
    mem[PATCH_VAR_SOURCE_U].uint = size;
    mem[PATCH_VAR_IN].uint = 0;
}

static bool hasinput (cell * mem)
{
    return mem[PATCH_VAR_IN].uint < mem[PATCH_VAR_SOURCE_U].uint;
}

static const char * input (cell * mem)
{
    return mem[PATCH_VAR_SOURCE].cstr + mem[PATCH_VAR_IN].uint;
}

static const char * inputend (cell * mem)
{
    return mem[PATCH_VAR_SOURCE].cstr + mem[PATCH_VAR_SOURCE_U].uint;
}

static size_t inputadvance (cell * mem, const char * ptr)
{
    // TODO: check overflow
    return mem[PATCH_VAR_IN].uint = ptr - mem[PATCH_VAR_SOURCE].cstr;
}

static void branch (cell * mem)
{
    mem[PATCH_VAR_IP] = *mem[PATCH_VAR_IP].cell;
}
static void branch0 (cell * mem)
{
    if (0 == ppop(mem)->sint)
    {
        branch(mem);
    }
    else
    {
        (void) next(mem);
    }
}
static void lit (cell * mem)
{
    *ppush(mem) = *next(mem);
}
static void enter (cell * mem)
{
    *rpush(mem) = mem[PATCH_VAR_IP];

    mem[PATCH_VAR_IP] = (cell) {.cell=mem[PATCH_VAR_W].cell};
}
static void execute (cell * mem)
{
    cell * code = ppop(mem)->cell;

    mem[PATCH_VAR_W] = (cell) {.cell=code + 1};
    code->op(mem);
}
static void exit_ (cell * mem)
{
    mem[PATCH_VAR_IP] = *rpop(mem);
}
PRIMITIVE_START("branch", branch, FLAG_COMP)
PRIMITIVE_ADD("?branch", branch0, FLAG_COMP, branch)
PRIMITIVE_ADD("[']", lit, FLAG_COMP, branch0)
PRIMITIVE_ADD("execute", execute, 0, lit)
PRIMITIVE_ADD("exit", exit_, FLAG_COMP, execute)

static void last (cell * mem)
{
    *ppush(mem) = (cell) {.cell=mem + PATCH_VAR_LAST};
}
static void state (cell * mem)
{
    *ppush(mem) = (cell) {.cell=mem + PATCH_VAR_STATE};
}
static void source (cell * mem)
{
    *ppush(mem) = mem[PATCH_VAR_SOURCE];
    *ppush(mem) = mem[PATCH_VAR_SOURCE_U];
}
static void in (cell * mem)
{
    *ppush(mem) = (cell) {.cell=mem + PATCH_VAR_IN};
}
static void base (cell * mem)
{
    *ppush(mem) = (cell) {.cell=mem + PATCH_VAR_BASE};
}
PRIMITIVE_ADD("last", last, 0, exit_)
PRIMITIVE_ADD("state", state, 0, last)
PRIMITIVE_ADD("source", source, 0, state)
PRIMITIVE_ADD(">in", in, 0, source)
PRIMITIVE_ADD("base", base, 0, in)

static void variable (cell * mem)
{
    *ppush(mem) = mem[PATCH_VAR_W];
}
static void constant (cell * mem)
{
    *ppush(mem) = (cell) {.cell=mem[PATCH_VAR_W].cell->cell};
}
static void create (cell * mem)
{
    cell * data = mem[PATCH_VAR_W].cell;

    *ppush(mem) = (cell) {.cell=data + 1};

    if (data->obj)
    {
        *rpush(mem) = mem[PATCH_VAR_IP];

        mem[PATCH_VAR_IP] = (cell) {.cell=data->cell};
    }
}

static cell * entry_next (cell * entry)
{
    return entry->cell;
}
static cell * entry_flags (cell * entry)
{
    return entry + 1;
}
static bool entry_imme (cell * entry)
{
    return entry_flags(entry)->uint & FLAG_IMME;
}
static bool entry_comp (cell * entry)
{
    return entry_flags(entry)->uint & FLAG_COMP;
}
static size_t entry_name_size (cell * entry)
{
    return entry_flags(entry)->uint & (FLAG_NAME | FLAG_HIDE);
}
static cell * entry_name (cell * entry)
{
    return entry + 2;
}
static const cell * entry_xt (const cell * entry)
{
    return entry + 3;
}
static cell * entry_data (cell * entry)
{
    return entry + 4;
}
static bool entry_mismatch (cell * entry, const char * name, size_t size)
{
    return entry && ((entry_name_size(entry) != size) ||
           memcmp(entry_name(entry)->str, name, size));
}
static cell * xt_entry (cell * xt)
{
    return xt - 3;
}

static bool state_interpretation (cell * mem)
{
    return STATE_INTERPR == mem[PATCH_VAR_STATE].uint;
}

static void leading (cell * mem)
{
    const char * beg = input(mem);
    const char * end = inputend(mem);

    while (beg < end && ('\0' == *beg || isspace(*beg))) beg += 1;

    inputadvance(mem, beg);
}

static void parse_name (cell * mem)
{
    const char * beg = input(mem);
    const char * end = inputend(mem);

    while (beg < end && ('\0' == *beg || isspace(*beg))) beg += 1;

    const char * ptr = beg;

    while (ptr < end && *ptr && !isspace(*ptr)) ptr += 1;

    ppush(mem)->cstr = beg;
    ppush(mem)->uint = ptr - beg;

    if (ptr < end) ptr += 1;

    inputadvance(mem, ptr);
}

static cell * find (cell * mem, const char * str, size_t size)
{
    cell * result = mem[PATCH_VAR_LAST].cell;

    while (entry_mismatch(result, str, size))
    {
        result = entry_next(result);
    }

    return result;
}

// XXX: we don't have counted string concept here
static void find_ (cell * mem)
{
    cell * val1 = ppeek(mem, 0);

    cell * result = find(mem, val1->str + 1, *val1->str);

    if (result)
    {
        val1->cobj = entry_xt(result);

        ppush(mem)->uint = entry_imme(result) ? 1 : -1;
    }
    else
    {
        ppush(mem)->uint = 0;
    }
}

static uintptr_t digit_value (char digit)
{
    if (isdigit(digit))
    {
        return digit - '0';
    }

    if (isupper(digit))
    {
        return digit - 'A' + 10;
    }

    if (islower(digit))
    {
        return digit - 'a' + 10;
    }

    return UINTPTR_MAX;
}

static bool number (uintptr_t base, const char * str, size_t size, cell * value)
{
    const char * end = str + size;

    uintptr_t result = 0;
    bool negative = false;

    if ('-' == *str)
    {
        str += 1;
        negative = true;
    }
    else if ('+' == *str)
    {
        str += 1;
    }

    if (str >= end)
    {
        value->uint = 0;
        return false;
    }

    while (str < end)
    {
        char digit = digit_value(*str++);

        if (digit < base)
        {
            result *= base;
            result += digit;
        }
        else
        {
            value->uint = 0;
            return false;
        }
    }

    if (negative)
    {
        value->sint = -(intptr_t) result;
    }
    else
    {
        value->uint = result;
    }

    return true;
}

static void openbra (cell * mem)
{
    mem[PATCH_VAR_STATE] = (cell) {.uint=STATE_INTERPR};
}
static void closebra (cell * mem)
{
    mem[PATCH_VAR_STATE] = (cell) {.uint=STATE_COMPILE};
}
static void immediate (cell * mem)
{
    cell * flags = entry_flags(mem[PATCH_VAR_LAST].cell);
    flags->uint = flags->uint | FLAG_IMME;
}
static void compile_only (cell * mem)
{
    cell * flags = entry_flags(mem[PATCH_VAR_LAST].cell);
    flags->uint = flags->uint | FLAG_COMP;
}
static void build (cell * mem)
{
    parse_name(mem);

    size_t size = ppop(mem)->uint;
    const char * str = ppop(mem)->cstr;

    if (!size || (size > FLAG_NAME))
    {
        patch_user_error(mem, ERROR_0_LEN_NAME);
    }

    const char * str_ = here(mem);

    compile_str(mem, str, size);

    align(mem);

    const void * entry = here(mem);

    compile(mem)->cell = mem[PATCH_VAR_LAST].cell;
    compile(mem)->uint = size;
    compile(mem)->cstr = str_;

    mem[PATCH_VAR_LAST] = (cell) {.cobj=entry};
}
static void hide (cell * mem)
{
    cell * flags = entry_flags(ppop(mem)->cell);
    flags->uint = flags->uint | FLAG_HIDE;
}
static void reveal (cell * mem)
{
    cell * flags = entry_flags(ppop(mem)->cell);
    flags->uint = flags->uint & ~FLAG_HIDE;
}
static void colon (cell * mem)
{
    build(mem);
    compile(mem)->op = enter;
    closebra(mem);

    // TODO: better?
    ppush(mem)->cell = mem[PATCH_VAR_LAST].cell;
    hide(mem);
}
static void semicolon (cell * mem)
{
    compile(mem)->cobj = entry_xt(exit__word);
    openbra(mem);

    // TODO: better?
    ppush(mem)->cell = mem[PATCH_VAR_LAST].cell;
    reveal(mem);
}
static void variable_ (cell * mem)
{
    build(mem);
    compile(mem)->op = variable;
    (void) compile(mem);
}
static void constant_ (cell * mem)
{
    build(mem);
    compile(mem)->op = constant;
    compile(mem)->cell = ppop(mem)->cell;
}
static void create_ (cell * mem)
{
    build(mem);
    compile(mem)->op = create;
    compile(mem)->cell = NULL;
}
static void does_ (cell * mem)
{
    cell * data = entry_data(mem[PATCH_VAR_LAST].cell);

    data->cell = mem[PATCH_VAR_IP].cell;

    exit_(mem);
}
static void postpone (cell * mem)
{
    cell * xt = next(mem)->cell;

    if (entry_imme(xt_entry(xt)))
    {
        ppush(mem)->cell = xt;
        execute(mem);
    }
    else
    {
        compile(mem)->cell = xt;
    }
}
static const cell postpone_[] = {{.op=postpone}};
static void postpone__ (cell * mem)
{
    parse_name(mem);

    size_t size = ppop(mem)->uint;
    const char * str = ppop(mem)->cstr;

    // TODO: make find return xt
    cell * entry = find(mem, str, size);

    if (entry)
    {
        compile(mem)->cobj = postpone_;
        compile(mem)->cobj = entry_xt(entry);
    }
    else
    {
        patch_user_error(mem, ERROR_UNDEF_WORD);
    }
}
static void comment (cell * mem)
{
    const char * beg = input(mem);
    const char * end = inputend(mem);

    while (beg < end && ('\0' != *beg && '\n' != *beg && '\r' != *beg))
    {
        beg += 1;
    }

    inputadvance(mem, beg);
}
static void opencom (cell * mem)
{
    const char * beg = input(mem);
    const char * end = inputend(mem);

    while (beg < end && ('\0' != *beg && ')' != *beg))
    {
        beg += 1;
    }

    if (beg < end) beg += 1;

    inputadvance(mem, beg);
}
PRIMITIVE_ADD("find", find_, 0, base)
PRIMITIVE_ADD("leading-", leading, 0, find_)
PRIMITIVE_ADD("parse-name", parse_name, 0, leading)
PRIMITIVE_ADD("[", openbra, FLAG_IMME, parse_name)
PRIMITIVE_ADD("]", closebra, 0, openbra)
PRIMITIVE_ADD("immediate", immediate, 0, closebra)
PRIMITIVE_ADD("compile-only", compile_only, 0, immediate)
PRIMITIVE_ADD("<build", build, 0, compile_only)
PRIMITIVE_ADD(":", colon, 0, build)
PRIMITIVE_ADD(";", semicolon, FLAG_IMME, colon)
PRIMITIVE_ADD("variable", variable_, 0, semicolon)
PRIMITIVE_ADD("constant", constant_, 0, variable_)
PRIMITIVE_ADD("create", create_, 0, constant_)
// XXX: compile-only: for now?
PRIMITIVE_ADD("does>", does_, FLAG_COMP, create_)
PRIMITIVE_ADD("postpone", postpone__, FLAG_COMP | FLAG_IMME, does_)
PRIMITIVE_ADD("\\", comment, FLAG_IMME, postpone__)
PRIMITIVE_ADD("(", opencom, FLAG_IMME, comment)

static void abort_ (cell * mem)
{
    patch_user_error(mem, ERROR_ABORT);
}
PRIMITIVE_ADD("abort", abort_, 0, opencom)

static void here_ (cell * mem)
{
    ppush(mem)->str = here(mem);
}
static void comma (cell * mem)
{
    compile(mem)->cell = ppop(mem)->cell;
}
static void comma_byte (cell * mem)
{
    *compile_byte(mem) = ppop(mem)->byte;
}
static void allot_ (cell * mem)
{
    (void) allot(mem, ppop(mem)->sint);
}
static void aligned_ (cell * mem)
{
    *ppeek(mem, 0) = aligned(*ppeek(mem, 0));
}
PRIMITIVE_ADD("here", here_, 0, abort_)
PRIMITIVE_ADD(",", comma, 0, here_)
PRIMITIVE_ADD("c,", comma_byte, 0, comma)
PRIMITIVE_ADD("allot", allot_, 0, comma_byte)
PRIMITIVE_ADD("aligned", aligned_, 0, allot_)
PRIMITIVE_ADD("align", align, 0, aligned_)

static void move (cell * mem)
{
    cell * addr1 = ppop_frame(mem, 3) + 1;
    cell * addr2 = addr1 + 1;
    cell * size =  addr2 + 1;

    memmove(addr2->obj, addr1->obj, size->uint);
}
static void fetch (cell * mem)
{
    ppeek(mem, 0)->cell = ppeek(mem, 0)->cell->cell;
}
static void store (cell * mem)
{
    cell * addr = ppop(mem);
    cell * val = ppop(mem);

    addr->cell->cell = val->cell;
}
static void add_to (cell * mem)
{
    cell * addr = ppop(mem);
    cell * val = ppop(mem);

    addr->cell->uint += val->uint;
}
static void fetch_byte (cell * mem)
{
    ppeek(mem, 0)->uint = ppeek(mem, 0)->cell->ubyte;
}
static void store_byte (cell * mem)
{
    cell * addr = ppop(mem);
    cell * val = ppop(mem);

    addr->cell->byte = val->byte;
}
PRIMITIVE_ADD("move", move, 0, align)
PRIMITIVE_ADD("@", fetch, 0, move)
PRIMITIVE_ADD("!", store, 0, fetch)
PRIMITIVE_ADD("+!", add_to, 0, store)
PRIMITIVE_ADD("c@", fetch_byte, 0, add_to)
PRIMITIVE_ADD("c!", store_byte, 0, fetch_byte)

#define BINARY_OP(op, name, label, type, prev) \
static void label (cell * mem) \
{ \
    cell * val2 = ppop(mem); \
    cell * val1 = ppeek(mem, 0); \
    val1->type = val1->type op val2->type; \
} \
PRIMITIVE_ADD(name, label, 0, prev)

BINARY_OP(+, "+", add, uint, store_byte)
BINARY_OP(-, "-", sub, uint, add)
BINARY_OP(*, "*", mul, sint, sub)
BINARY_OP(>>, "rshift", rshift, uint, mul)
BINARY_OP(<<, "lshift", lshift, uint, rshift)
BINARY_OP(&, "and", and_, uint, lshift)
BINARY_OP(|, "or", or_, uint, and_)
BINARY_OP(^, "xor", xor_, uint, or_)

#define BINARY_COMP_OP(op, name, label, type, prev) \
static void label (cell * mem) \
{ \
    cell * val2 = ppop(mem); \
    cell * val1 = ppeek(mem, 0); \
    val1->sint = boolfth(val1->type op val2->type); \
} \
PRIMITIVE_ADD(name, label, 0, prev)

BINARY_COMP_OP(<=, "<=", le, sint, xor_)
BINARY_COMP_OP(>=, ">=", ge, sint, le)
BINARY_COMP_OP(<=, "u<=", ule, uint, ge)
BINARY_COMP_OP(>=, "u>=", uge, uint, ule)
BINARY_COMP_OP(<, "<", lt, sint, uge)
BINARY_COMP_OP(>, ">", gt, sint, lt)
BINARY_COMP_OP(<, "u<", ult, uint, gt)
BINARY_COMP_OP(>, "u>", ugt, uint, ult)
BINARY_COMP_OP(==, "=", eq, sint, ugt)
BINARY_COMP_OP(!=, "<>", ne, sint, eq)

#define UNARY_OP(op, name, label, type, prev) \
static void label (cell * mem) \
{ \
    ppeek(mem, 0)->uint = op ppeek(mem, 0)->uint; \
} \
PRIMITIVE_ADD(name, label, 0, prev)

UNARY_OP(-, "negate", negate, sint, ne)
UNARY_OP(~, "invert", invert, uint, negate)

static void by2 (cell * mem)
{
    cell * val = ppeek(mem, 0);

    val->sint = val->sint >> 1;
}
static void divmod (cell * mem)
{
    cell * val1 = ppeek(mem, 1);
    cell * val2 = ppeek(mem, 0);

    check(mem, 0 == val2->uint, ERROR_0_DIV);

    cell res1 = (cell) {.sint=val1->sint % val2->sint};
    cell res2 = (cell) {.sint=val1->sint / val2->sint};
    *val1 = res1;
    *val2 = res2;
}
static dcell mmul (cell val1, cell val2)
{
    dcell dval1 = (dcell) {.sint=val1.sint};
    dcell dval2 = (dcell) {.sint=val2.sint};
    return (dcell) {.sint=dval1.sint * dval2.sint};
}
static void mmul_ (cell * mem)
{
    cell * vals = ppeek(mem, 1);

    dcell result = mmul(vals[0], vals[1]);

    vals[0] = result.cell[0];
    vals[1] = result.cell[1];
}
static dcell ummul (cell val1, cell val2)
{
    dcell dval1 = (dcell) {.uint=val1.uint};
    dcell dval2 = (dcell) {.uint=val2.uint};
    return (dcell) {.sint=dval1.uint * dval2.uint};
}
static void ummul_ (cell * mem)
{
    cell * vals = ppeek(mem, 1);

    dcell result = ummul(vals[0], vals[1]);

    vals[0] = result.cell[0];
    vals[1] = result.cell[1];
}
static dcell fmdivmod (dcell val1, cell val2)
{
    cell rem  = (cell) {.sint=val1.sint % val2.sint};
    cell quot = (cell) {.sint=val1.sint / val2.sint};

    if ((rem.sint != 0) && ((rem.sint < 0) != (val2.sint < 0)))
    {
        rem.sint += val2.sint;
        quot.sint -= 1;
    }

    return (dcell) {.cell={rem, quot}};
}
static void fmdivmod_ (cell * mem)
{
    cell * divider = ppop(mem);
    cell * frame = ppeek(mem, 1);

    check(mem, 0 == divider->sint, ERROR_0_DIV);

    dcell result = fmdivmod((dcell){.cell={frame[0],frame[1]}},*divider);

    frame[0] = result.cell[0];
    frame[1] = result.cell[1];
}
static dcell smdivrem (dcell val1, cell val2)
{
    return (dcell) {.cell={{val1.sint % val2.sint}, {val1.sint / val2.sint}}};
}
static void smdivrem_ (cell * mem)
{
    cell * divider = ppop(mem);
    cell * frame = ppeek(mem, 1);

    check(mem, 0 == divider->sint, ERROR_0_DIV);

    dcell result = smdivrem((dcell){.cell={frame[0],frame[1]}},*divider);

    frame[0] = result.cell[0];
    frame[1] = result.cell[1];
}
static dcell umdivmod (dcell val1, cell val2)
{
    return (dcell) {.cell={{val1.uint % val2.uint}, {val1.uint / val2.uint}}};
}
static void umdivmod_ (cell * mem)
{
    cell * divider = ppop(mem);
    cell * frame = ppeek(mem, 1);

    check(mem, 0 == divider->uint, ERROR_0_DIV);

    dcell result = umdivmod((dcell){.cell={frame[0],frame[1]}},*divider);

    frame[0] = result.cell[0];
    frame[1] = result.cell[1];
}
static void dnegate (cell * mem)
{
    cell * frame = ppeek(mem, 1);

    dcell result = (dcell){.cell={frame[0], frame[1]}};
    result.sint -= result.sint;

    frame[0] = result.cell[0];
    frame[1] = result.cell[1];
}
static void mplus (cell * mem)
{
    cell arg = *ppop(mem);

    cell * frame = ppeek(mem, 1);

    dcell result = (dcell){.cell={frame[0], frame[1]}};
    result.uint += arg.uint;

    frame[0] = result.cell[0];
    frame[1] = result.cell[1];
}
PRIMITIVE_ADD("2/", by2, 0, invert)
PRIMITIVE_ADD("m*", mmul_, 0, by2)
PRIMITIVE_ADD("um*", ummul_, 0, mmul_)
PRIMITIVE_ADD("/mod", divmod, 0, ummul_)
PRIMITIVE_ADD("fm/mod", fmdivmod_, 0, divmod)
PRIMITIVE_ADD("sm/rem", smdivrem_, 0, fmdivmod_)
PRIMITIVE_ADD("um/mod", umdivmod_, 0, smdivrem_)
PRIMITIVE_ADD("dnegate", dnegate, 0, umdivmod_)
PRIMITIVE_ADD("m+", mplus, 0, dnegate)

static void cells_ (cell * mem)
{
    ppeek(mem, 0)->uint = sizeof(cell) * ppeek(mem, 0)->uint;
}
PRIMITIVE_ADD("cells", cells_, 0, mplus)

static void depth (cell * mem)
{
    uintptr_t value = patch_depth(mem);

    ppush(mem)->uint = value;
}
static void drop (cell * mem)
{
    (void) ppop(mem);
}
static void nip (cell * mem)
{
    *ppeek(mem, 1) = *ppeek(mem, 0);
    drop(mem);
}
static void swap (cell * mem)
{
    cell * val1 = ppeek(mem, 1);
    cell * val2 = ppeek(mem, 0);
    cell   tmp  = *val2;
    *val2 = *val1;
    *val1 = tmp;
}
static void dupmaybe (cell * mem)
{
    cell * val = ppeek(mem, 0);

    if (val->uint)
    {
        *ppush(mem) = *val;
    }
}
static void dup (cell * mem)
{
    cell * val = ppeek(mem, 0);
    *ppush(mem) = *val;
}
static void over (cell * mem)
{
    cell * val = ppeek(mem, 1);
    *ppush(mem) = *val;
}
static void pick (cell * mem)
{
    cell * val = ppeek(mem, 0);
    *val = *ppeek(mem, val->uint + 1);
}
static void tor (cell * mem)
{
    *rpush(mem) = *ppop(mem);
}
static void fromr (cell * mem)
{
    *ppush(mem) = *rpop(mem);
}
static void rfetch (cell * mem)
{
    *ppush(mem) = *rpeek(mem, 0);
}
static void rdrop (cell * mem)
{
    (void) rpop(mem);
}
PRIMITIVE_ADD("depth", depth, 0, cells_);
PRIMITIVE_ADD("drop", drop, 0, depth);
PRIMITIVE_ADD("nip", nip, 0, drop);
PRIMITIVE_ADD("swap", swap, 0, nip);
PRIMITIVE_ADD("?dup", dupmaybe, 0, swap);
PRIMITIVE_ADD("dup", dup, 0, dupmaybe);
PRIMITIVE_ADD("over", over, 0, dup);
PRIMITIVE_ADD("pick", pick, 0, over);
PRIMITIVE_ADD(">r", tor, 0, pick);
PRIMITIVE_ADD("r>", fromr, 0, tor);
PRIMITIVE_ADD("r@", rfetch, 0, fromr);
PRIMITIVE_ADD("rdrop", rdrop, 0, rfetch);

static void evaluate (cell * mem)
{
    // XXX: but this is wrong, single stepping should be possible or something
    //      I don't know
    //      Turns out I will have to rethink API as well

    *rpush(mem) = mem[PATCH_VAR_SOURCE];
    *rpush(mem) = mem[PATCH_VAR_SOURCE_U];
    *rpush(mem) = mem[PATCH_VAR_IN];
    *rpush(mem) = mem[PATCH_VAR_IP];

    size_t size = ppop(mem)->uint;

    // so disgusting xD
    // don't allow the machine take over itself :<
    patch_dostr(mem, ppop(mem)->cstr, size);

    mem[PATCH_VAR_IP] = *rpop(mem);
    mem[PATCH_VAR_IN] = *rpop(mem);
    mem[PATCH_VAR_SOURCE_U] = *rpop(mem);
    mem[PATCH_VAR_SOURCE] = *rpop(mem);
}
PRIMITIVE_ADD("evaluate", evaluate, 0, rdrop)

static void ccall (cell * mem)
{
    cell * ccall_ = ppop(mem);

    size_t argc = ppop(mem)->uint;

    (void) ppop_frame(mem, argc);

    *ppush(mem) = patch_ccall_1(ccall_);
}
PRIMITIVE_ADD("ccall", ccall, 0, evaluate)

void patch_push (cell * buffer, cell value)
{
    *ppush(buffer) = value;
}

cell patch_pop (cell * buffer)
{
    return *ppop(buffer);
}

size_t patch_depth (cell * buffer)
{
    return 1 + (buffer[PATCH_VAR_PSP].cell - buffer[PATCH_VAR_PS0].cell);
}

void * patch_alloc (cell * buffer, size_t size)
{
    if (size > 1)
    {
        align(buffer);
    }

    return allot(buffer, size);
}

void patch_doxt (cell * buffer, const cell * xt)
{
    buffer[PATCH_VAR_IP] = (cell) {.cell=NULL};

    goto doxt;

    while (buffer[PATCH_VAR_IP].cell)
    {
        xt = next(buffer)->cell;

doxt:
        buffer[PATCH_VAR_W] = (cell) {.cobj=xt + 1};
        xt->op(buffer);
    }
}

void patch_dostr (cell * buffer, const char * str, size_t size)
{
    setinput(buffer, str, size);

    while (hasinput(buffer))
    {
        parse_name(buffer);

        size = ppop(buffer)->uint;
        str = ppop(buffer)->cstr;

        if (!size)
        {
            break;
        }

        cell * found = find(buffer, str, size);

        if (found)
        {
            if (state_interpretation(buffer))
            {
                if (entry_comp(found))
                {
                    patch_user_error(buffer, ERROR_CONLY_WORD);
                }
                else
                {
                    patch_doxt(buffer, entry_xt(found));
                }
            }
            else
            {
                if (entry_imme(found))
                {
                    patch_doxt(buffer, entry_xt(found));
                }
                else
                {
                    compile(buffer)->cobj = entry_xt(found);
                }
            }
        }
        else
        {
            cell value;

            // TODO: base
            if (number(buffer[PATCH_VAR_BASE].uint, str, size, &value))
            {
                if (state_interpretation(buffer))
                {
                    *ppush(buffer) = value;
                }
                else
                {
                    compile(buffer)->cobj = entry_xt(lit_word);
                    compile(buffer)->uint = value.uint;
                }
            }
            else
            {
                patch_user_error(buffer, ERROR_UNDEF_WORD);
            }
        }
    }
}

PRIMITIVE_ADD("empty", patch_empty, 0, ccall)

void patch_empty (cell * buffer)
{
    buffer[PATCH_VAR_PSP].cell = buffer + PATCH_VAR_PS_POS;
    buffer[PATCH_VAR_RSP].cell = buffer + PATCH_VAR_RS_POS;
    buffer[PATCH_VAR_DP].cell  = buffer + PATCH_VAR_DATA_SPACE_START;
    buffer[PATCH_VAR_IP].cell = NULL;
    buffer[PATCH_VAR_LAST].cobj = patch_empty_word; // TODO: MUST the last word in dictionary
    buffer[PATCH_VAR_BASE].uint = 10;
    openbra(buffer);
}

void patch_init (cell * buffer, size_t size)
{
    buffer[PATCH_VAR_PS0].cell = buffer + PATCH_VAR_PS_BEG;
    buffer[PATCH_VAR_PSP].cell = buffer + PATCH_VAR_PS_POS;
    buffer[PATCH_VAR_PSE].cell = buffer + PATCH_VAR_PS_END;
    buffer[PATCH_VAR_RS0].cell = buffer + PATCH_VAR_RS_BEG;
    buffer[PATCH_VAR_RSP].cell = buffer + PATCH_VAR_RS_POS;
    buffer[PATCH_VAR_RSE].cell = buffer + PATCH_VAR_RS_END;
    buffer[PATCH_VAR_DP].cell  = buffer + PATCH_VAR_DATA_SPACE_START;
    buffer[PATCH_VAR_DSIZE].uint = size;
    buffer[PATCH_VAR_IP].cell = NULL;
    buffer[PATCH_VAR_LAST].cobj = patch_empty_word; // TODO: MUST the last word in dictionary
    buffer[PATCH_VAR_BASE].uint = 10;
    openbra(buffer);
}

#endif // PATCH_IMPL

#ifdef PATCH_CCALL_IMPL
#if __x86_64 && __unix

typedef struct patch_dcell_t
{
    patch_cell_t lo;
    patch_cell_t hi;
}
patch_dcell_t;

patch_dcell_t patch_ccall_2 (patch_cell_t * stack);

__asm(
".global patch_ccall\n"
".global patch_ccall_1\n"
".global patch_ccall_2\n"
"\n"
"patch_ccall:\n"
"patch_ccall_1:\n"
"patch_ccall_2:\n"
"\n"
"    mov     %rdi, %r11\n"
"    lea     -8(%rdi), %r10\n"
"    mov     (%r10), %rcx\n"
"    mov     %rcx, %rsi\n"
"    shl     $0x03, %rsi\n"
"    sub     %rsi, %r10\n"
"\n"
"    // setup parameters on stack (if needed)\n"
"    cmp     $6, %rcx\n"
"    jg      2f\n"
"\n"
"    lea     1f(%rip), %r8\n"
"    mov     $6, %r9\n"
"    sub     %rcx, %r9\n"
"    lea     (%r8,%r9,4), %r8\n"
"    jmp     *%r8\n"
"\n"
"1:\n"
"    mov     40(%r10), %r9\n"
"    mov     32(%r10), %r8\n"
"    mov     24(%r10), %rcx\n"
"    mov     16(%r10), %rdx\n"
"    mov      8(%r10), %rsi\n"
"    mov      0(%r10), %rdi\n"
"    nop\n"
"\n"
"    xor %rax, %rax\n"
"\n"
"    jmp *(%r11)\n"
"\n"
"2:\n"
"    push    %rbp\n"
"    mov     %rsp, %rbp\n"
"\n"
"    // parameter count (that don't fit regs)\n"
"    lea     -6(%rcx), %rcx\n"
"    // parameter source\n"
"    lea     48(%r10), %rsi\n"
"    // allocate space for params + 1, then align\n"
"    lea     8(,%rcx,8), %rdx\n"
"    sub     %rdx, %rsp\n"
"    and     $-15, %rsp\n"
"    // parameter destination\n"
"    mov     %rsp, %rdi\n"
"    // copy\n"
"    rep     movsq\n"
"\n"
"    call    1b\n"
"    leave\n"
"    ret\n"
);
#elif __x86_64 && __WIN64
__asm(
".global patch_ccall\n"
".global patch_ccall_1\n"
"\n"
"patch_ccall:\n"
"patch_ccall_1:\n"
"\n"
"    mov     %rcx, %r11\n"
"    lea     -8(%rcx), %r10\n"
"    mov     (%r10), %rcx\n"
"    mov     %rcx, %rdx\n"
"    shl     $0x03, %rdx\n"
"    sub     %rdx, %r10\n"
"\n"
"    // setup parameters on stack (if needed)\n"
"    cmp     $4, %rcx\n"
"    jg      2f\n"
"\n"
"    lea     1f(%rip), %r8\n"
"    mov     $4, %r9\n"
"    sub     %rcx, %r9\n"
"    lea     (%r8,%r9,4), %r8\n"
"    jmp     *%r8\n"
"\n"
"1:\n"
"    mov     24(%r10), %r9\n"
"    mov     16(%r10), %r8\n"
"    mov      8(%r10), %rdx\n"
"    mov      0(%r10), %rcx\n"
"    nop\n"
"\n"
"    jmp *(%r11)\n"
"\n"
"2:\n"
"    push    %rbp\n"
"    mov     %rsp, %rbp\n"
"\n"
"    mov     %rdi, %r8\n"
"    mov     %rsi, %r9\n"
"\n"
"    // parameter count (that don't fit regs)\n"
"    lea     -4(%rcx), %rcx\n"
"    // parameter source\n"
"    lea     32(%r10), %rsi\n"
"    // allocate space for params + spill area + 1, then align\n"
"    lea     8(%rdx), %rdx\n"
"    sub     %rdx, %rsp\n"
"    and     $-15, %rsp\n"
"    // parameter destination\n"
"    lea     32(%rsp), %rdi\n"
"    // copy\n"
"    rep     movsq\n"
"\n"
"    mov     %r8, %rdi\n"
"    mov     %r9, %rsi\n"
"\n"
"    call    1b\n"
"    leave\n"
"    ret\n"
);
#elif __i386 && __unix

typedef struct patch_dcell_t
{
    patch_cell_t lo;
    patch_cell_t hi;
}
patch_dcell_t;

patch_dcell_t patch_ccall_2 (patch_cell_t * stack);

__asm(
".global patch_ccall\n"
".global patch_ccall_1\n"
".global patch_ccall_2\n"
"\n"
"patch_ccall:\n"
"patch_ccall_1:\n"
"patch_ccall_2:\n"
"\n"
"    push    %edi\n"
"    push    %esi\n"
"    push    %ebp\n"
"    mov     %esp, %ebp\n"
"\n"
"    mov     16(%ebp), %eax\n"
"    mov     -4(%eax), %ecx\n"
"    lea     (,%ecx,4), %edi\n"
"    sub     %edi, %esp\n"
"    lea     -4(%eax), %esi\n"
"    sub     %edi, %esi\n"
"    mov     %esp, %edi\n"
"    rep     movsd\n"
"\n"
"    call *(%eax)\n"
"\n"
"    leave\n"
"    pop     %esi\n"
"    pop     %edi\n"
"    ret\n"
);
#elif __ARM

typedef uint64_t patch_dcell_t;

patch_dcell_t patch_ccall_2 (patch_cell_t * stack);

__asm(
".global patch_ccall\n"
".global patch_ccall_1\n"
".global patch_ccall_2\n"
"\n"
"patch_ccall:\n"
"patch_ccall_1:\n"
"patch_ccall_2:\n"
"\n"
"    push { r7, lr }\n"
"    mov r7, sp\n"
"\n"
"    # r0 -- ccall\n"
"\n"
"    ldr r1, [r0]\n"
"    mov ip, r1\n"
"    sub r0, #4\n"
"    ldr r1, [r0]\n"
"    lsl r1, #2\n"
"    sub r0, #4\n"
"\n"
"    # r0 -- last arg\n"
"    # r1 -- param size\n"
"    # ip -- function\n"
"\n"
"    cmp r1, #0\n"
"    beq 0f\n"
"    cmp r1, #4\n"
"    beq 1f\n"
"    cmp r1, #8\n"
"    beq 2f\n"
"    cmp r1, #12\n"
"    beq 3f\n"
"    cmp r1, #16\n"
"    beq 4f\n"
"\n"
"    sub r1, #16\n"
"    sub r1, r0, r1\n"
"\n"
"5:\n"
"    cmp r0, r1\n"
"    beq 4f\n"
"    sub sp, #4\n"
"    ldr r2, [r0]\n"
"    str r2, [sp]\n"
"    sub r0, #4\n"
"    b 5b\n"
"\n"
"    @TODO: align to 8 bytes\n"
"\n"
"4:\n"
"    ldr r3, [r0]\n"
"    sub r0, #4\n"
"\n"
"3:\n"
"    ldr r2, [r0]\n"
"    sub r0, #4\n"
"\n"
"2:\n"
"    ldr r1, [r0]\n"
"    sub r0, #4\n"
"\n"
"1:\n"
"    ldr r0, [r0]\n"
"\n"
"0:\n"
"    blx ip\n"
"    mov sp, r7\n"
"    pop { r7, pc }\n"
);
#else
#   error Unknown target.
#endif
#endif // PATCH_CCALL_IMPL

#ifdef PATCH_LIB_DL

#include <dlfcn.h>

static void rtld_lazy (cell * mem)
{
    ppush(mem)->uint = RTLD_LAZY;
}
static void rtld_now (cell * mem)
{
    ppush(mem)->uint = RTLD_NOW;
}
static void rtld_global (cell * mem)
{
    ppush(mem)->uint = RTLD_GLOBAL;
}
static void rtld_local (cell * mem)
{
    ppush(mem)->uint = RTLD_LOCAL;
}
static void dlopen_ (cell * mem)
{
    uintptr_t flags = ppop(mem)->uint;

    cell * val = ppeek(mem, 0);

    val->obj = dlopen(val->cobj, flags);
}
static void dlclose_ (cell * mem)
{
    cell * val = ppeek(mem, 0);

    val->sint = dlclose(val->obj);
}
static void dlerror_ (cell * mem)
{
    ppush(mem)->cobj = dlerror();
}
static void dlsym_ (cell * mem)
{
    const char * name = ppop(mem)->cobj;

    cell * val = ppeek(mem, 0);

    val->op = dlsym(val->obj, name);
}

// FIXME: make patch_empty last (or some mechanism to have last word)
PRIMITIVE_ADD("RTLD_LAZY", rtld_lazy, 0, patch_empty)
PRIMITIVE_ADD("RTLD_NOW", rtld_now, 0, rtld_lazy)
PRIMITIVE_ADD("RTLD_GLOBAL", rtld_global, 0, rtld_now)
PRIMITIVE_ADD("RTLD_LOCAL", rtld_local, 0, rtld_global)
PRIMITIVE_ADD("dlopen", dlopen_, 0, rtld_local)
PRIMITIVE_ADD("dlclose", dlclose_, 0, dlopen_)
PRIMITIVE_ADD("dlerror", dlerror_, 0, dlclose_)
PRIMITIVE_ADD("dlsym", dlsym_, 0, dlerror_)

#endif // PATCH_LIB_DL

#ifdef PATCH_LIB_LUA

#include <lauxlib.h>

static int lpatchdostr (lua_State * L)
{
    size_t size;

    patch_cell_t * mem = luaL_checkudata(L, 1, "patch");
    const char * str = luaL_checklstring(L, 2, &size);

    patch_dostr(mem, str, size);

    return 0;
}

static int lpatchpush (lua_State * L)
{
    patch_cell_t * mem = luaL_checkudata(L, 1, "patch");

    for (int i = 2, n = lua_gettop(L); i <= n; ++i)
    {
        // TODO: check first? push later?
        patch_push(mem, (cell) {.sint=luaL_checkinteger(L, i)});
    }

    return 0;
}

static int lpatchpop (lua_State * L)
{
    patch_cell_t * mem = luaL_checkudata(L, 1, "patch");

    int i = lua_gettop(L) > 1 ? luaL_checkinteger(L, 2) : 1;
    int n = i;

    while (i--)
    {
        lua_pushinteger(L, patch_pop(mem).sint);
    }

    return n;
}

static int lpatchdepth (lua_State * L)
{
    lua_pushinteger(L, patch_depth(luaL_checkudata(L, 1, "patch")));
    return 1;
}

static int lpatchempty (lua_State * L)
{
    patch_empty(luaL_checkudata(L, 1, "patch"));
    return 0;
}

static int lpatch (lua_State * L)
{
    size_t size = luaL_checkinteger(L, 1) * sizeof(patch_cell_t);

    patch_cell_t * mem = lua_newuserdata(L, size);

    luaL_setmetatable(L, "patch");

    patch_init(mem, size);

    return 1;
}

static const struct luaL_Reg reg[] = {
    { "__index",   NULL              },
    { "dostr",     lpatchdostr       },
    { "push",      lpatchpush        },
    { "pop",       lpatchpop         },
    { "depth",     lpatchdepth       },
    { "empty",     lpatchempty       },
    { NULL,        NULL              }
};

// TODO: add words from basic app here

int luaopen_patch (lua_State * L)
{
    if (luaL_newmetatable(L, "patch"))
    {
        luaL_setfuncs(L, reg, 0);
        lua_pushvalue(L, -1);
        lua_setfield(L, -2, "__index");
    }

    lua_pushcfunction(L, lpatch);
    return 1;
}

#endif // PATCH_LIB_LUA

#ifdef PATCH_APP_BASIC

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

static patch_cell_t memory[8192];

static char line[1024];

static void bye (cell * unused) { exit(0); }

PRIMITIVE_ADD("bye", bye, 0, dlsym_)

// TODO: more general output method
static void emit (patch_cell_t * mem)
{
    putchar(ppop(mem)->sint);
}
// TODO: rethink this appropach: how to expose that functionality to the client
PRIMITIVE_ADD("emit", emit, 0, bye)

// TODO: more general output method
static void type (patch_cell_t * mem)
{
    size_t size = ppop(mem)->uint;

    fwrite(ppop(mem)->cobj, size, 1, stdout);
}
// TODO: rethink this appropach: how to expose that functionality to the client
PRIMITIVE_ADD("type", type, 0, emit)

static void unused (patch_cell_t * mem)
{
    const char * end = ((const char *) mem) + mem[PATCH_VAR_DSIZE].uint;
    ppush(mem)->uint = end - mem[PATCH_VAR_DP].cstr;
}
PRIMITIVE_ADD("unused", unused, 0, type)

static const char * err_to_str (int code)
{
    switch (code)
    {
        case ERROR_ABORT:
        case ERROR_ABORT2:       return "abort";
        case ERROR_PS_OVERFLOW:  return "stack overflow";
        case ERROR_PS_UNDERFLOW: return "stack underflow";
        case ERROR_RS_OVERFLOW:  return "return stack overflow";
        case ERROR_RS_UNDERFLOW: return "return stack underflwo";
        case ERROR_DI_OVERFLOW:  return "dictionary overflow";
        case ERROR_0_DIV:        return "division by 0";
        case ERROR_UNDEF_WORD:   return "undefined word";
        case ERROR_CONLY_WORD:   return "interpreting a compile-only word";
        case ERROR_0_LEN_NAME:
                return "attempt to user zero-length string as a name";
        case ERROR_BAD_ALIGN:    return "address alignment exception";
        case ERROR_COMP_NESTING: return "compiler nesting";
        case ERROR_NO_BODY:      return ">BODY used on non-CREATEd definition";
        default:                 return "unknown error";
    }
}

// TODO: default implementation? So that it is easier for user to start, as in lua
noreturn void patch_user_error (patch_cell_t * mem, intptr_t code)
{
    fprintf(stderr, "patch: %s\n", err_to_str(code));
    exit(1);
}

// TODO: expose args to the vm
int main (int argc, char ** argv)
{
    patch_init(memory, sizeof(memory));

    // XXX, FIXME
    memory[PATCH_VAR_LAST].cobj = unused_word;

    while (fgets(line, sizeof(line), stdin))
    {
        patch_dostr(memory, line, strcspn(line, "\n\r"));
        fflush(stdout);
    }
}

#elif PATCH_APP_NONE

// no main

#else // PATCH_APP_BUILD - default

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NAME   "patch"
#define NAMEU  "PATCH"
#define CC     "gcc"
#define CFLAGS "-Wall -Os -march=native -D NDEBUG"
#define CFLAGS_DBG "-Wall -g -march=native"

int main (int argc, char **argv)
{
    char a = (argc == 2 && strlen(argv[1]) == 1) ? argv[1][0] : 'h';
    const char *p;

    switch(a) {
    case 'a': p =
        CC" "CFLAGS" -D"NAMEU"_IMPL -D"NAMEU"_CCALL_IMPL -D"NAMEU"_APP_NONE"
            " -c "NAME".c -lm -ldl &&\n"
        "strip --strip-unneeded "NAME".o &&\n"
        "ar rcs lib"NAME".a "NAME".o";
        break;
    case 's': p =
        CC" "CFLAGS" -D"NAMEU"_IMPL -D"NAMEU"_CCALL_IMPL -D"NAMEU"_APP_NONE"
            " -shared -fPIC"
            " -o lib"NAME".so "NAME".c &&\n"
        "strip lib"NAME".so";
        break;
    case 'l': p =
        CC" "CFLAGS" -D"NAMEU"_IMPL -D"NAMEU"_CCALL_IMPL -D"NAMEU"_APP_NONE"
            " -D"NAMEU"_LIB_LUA"
            " -shared -fPIC"
            " -o "NAME".so "NAME".c &&\n"
        "strip "NAME".so";
        break;
    case 'b': p =
        CC" "CFLAGS" -D"NAMEU"_IMPL -D"NAMEU"_CCALL_IMPL -D"NAMEU"_APP_BASIC"
            " -D"NAMEU"_LIB_DL"
            " -o "NAME" "NAME".c -lm -ldl &&\n"
        "strip "NAME;
        break;
    case 'g': p =
        CC" "CFLAGS_DBG" -D"NAMEU"_IMPL -D"NAMEU"_CCALL_IMPL"
            " -D"NAMEU"_APP_BASIC -D"NAMEU"_LIB_DL"
            " -o "NAME"dbg "NAME".c -lm -ldl";
        break;
    case 'h': default:
        fprintf(stderr,
            "a - static lib\n"
            "s - shared lib\n"
            "l - lua lib\n"
            "b - basic app\n"
            "g - debug app\n");
        return 1;
    }

    printf("%s\n", p);
    system(p);
}

#endif // APPS
