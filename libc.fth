: argc,  dup 0 >= if postpone literal exit then drop ; compile-only
: name   >in @ bl word count drop swap >in ! ; compile-only
: lib:   name RTLD_LAZY RTLD_GLOBAL or dlopen constant ;
: fun:   name dlsym :
         swap argc, \ compile argumentc count
         postpone literal postpone ccall \ compile function call
         0= if postpone drop then \ get rid of return value, if none expected
         postpone ; ; \ finish function definition
: var:   name dlsym constant ;

lib: libc.so.6

0 -1 libc.so.6 fun: printf
0 -1 libc.so.6 fun: fprintf
1  1 libc.so.6 fun: isspace
1  2 libc.so.6 fun: fopen
0  1 libc.so.6 fun: fclose
1  3 libc.so.6 fun: fgets
1  1 libc.so.6 fun: strerror
1  1 libc.so.6 fun: strlen

libc.so.6 var: errno
libc.so.6 var: stderr

: triple >r >r >r s" (%ld %ld %ld)" drop  r> r> r> 4 ;

4 7 8 triple printf cr
cr

: isspace? ." space: " dup . isspace if ." yes!" exit then ." no :<" ;

bl isspace? cr
10 isspace? cr
char a isspace? cr
cr

hex errno . cr decimal
errno @ . cr

: "          [char] " word 1+ ;
: read-only  s" r" drop ;
: errno.     stderr @ s" %s (%ld)" drop errno @ dup strerror swap
             4 fprintf ;
: opened?    ?dup if ." opened successfully" exit then
             errno. cr abort ;
: display    >r begin pad unused r@ fgets ?dup while dup strlen type repeat rdrop ;

" libc.fth" read-only fopen opened? cr
dup display
fclose

libc.so.6 dlclose bye
